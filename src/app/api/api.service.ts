import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl:string = `http://127.0.0.1:8000/api/chat/`
  constructor(private http:HttpClient) { }
  obj:any;

  newcontact = {
    'contactName':"raouf",
    'contactPhone':'675416614'
  }
  connection(data:string){
   return this.http.get(this.baseUrl+`${data}`,{observe:'body',responseType:'json'})
  }

  MyContacts(number:string){
    return this.http.get(this.baseUrl+`profile/${number}`);
    
  }

  postContact(owner:string,contact:any){
    this.http.post(this.baseUrl+`addContact/${owner}`,this.newcontact).subscribe((data)=>{
      console.log(data)
    })
  }

  readMessages(contact:string){
    let owner = localStorage.getItem('currentUser')
    return this.http.get(this.baseUrl+`${owner}/${contact}`);
  }

  
}
