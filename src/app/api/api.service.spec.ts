import { HttpClientModule, HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';

describe('api testing', () => {
  let service:ApiService;
  let httpMock:HttpClientTestingModule;

  beforeEach(() => {
    TestBed.configureTestingModule({ 
      imports: [HttpClientModule,HttpClientTestingModule], 
      providers: [ApiService] 
    });

    service = TestBed.get(ApiService);
    httpMock = TestBed.get(HttpTestingController)
  });

it("should be create",()=>{
  expect(service).toBeTruthy();
})

});
