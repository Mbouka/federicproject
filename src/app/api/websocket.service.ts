import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import io from 'socket.io-client';
Observable
@Injectable()
export class WebsocketService {

  private socket = io('http://localhost:8000');

  public listen(eventname:string){
    return new Observable((subscriber)=>{
      this.socket.on(eventname,(data)=>{
        subscriber.next(data)
      })
    })
  }

  newMessageReceived(){
    let observable = new Observable<any>(observer=>{
      this.socket.on('msg',function(data){
        observer.next(data);
      })
    })
    return observable;
  }

  public emit(eventname:string,data:any){
    this.socket.emit(eventname,data);
  }


}
