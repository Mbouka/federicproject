import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/api/api.service';
import { WebsocketService } from 'src/app/api/websocket.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  sender = localStorage.getItem('currentUser')
  receiver = this.route.snapshot.paramMap.get("e")
  obj:any;
  Messages:Array<any> = []

  constructor(private _api:ApiService,private route:ActivatedRoute,private websock:WebsocketService) {
    this.websock.listen("msg").subscribe(data=>{
      if((data["receiver"]==this.sender && data["sender"]==this.receiver) || data["receiver"]==this.receiver && data["sender"]==this.sender){
        this.Messages.push(data)
      }
    })

    console.log(this.Messages)
    
  }
 
  
  ngOnInit() {


    this.websock.emit('join',{contact:this.receiver})

    

  }

    

  submitdata(data){
    let details = {
      body:data.message,
      sender:this.sender,
      receiver:this.receiver,
    }

    this.websock.emit('new-msg',details)

  }



}

