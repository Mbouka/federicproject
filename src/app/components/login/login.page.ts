import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService} from 'src/app/api/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formdata;

  newMessage:string;
  messageList:string[] = []
  constructor(private api:ApiService,private router:Router) { }

  ngOnInit() {
  
  
  }
  onSubmit(data){
    this.api.MyContacts(data.userPhone).subscribe((e)=>{
      localStorage.setItem('currentUser',data.userPhone)
      this.router.navigate(['/test'])
    })
  }



}
