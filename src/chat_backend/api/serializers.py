from dataclasses import field, fields
from rest_framework import serializers

from .models import *




class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserModel
        fields = ['phone','firstname','pseudo']


class ContactSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = contactBook
        fields = ['contactPhone','contactName','owner']
        

        def to_representation(self,instance):
            response = super().to_representation(instance)
            response['owner'] = UserSerializer(instance.owner).data
            return response


class MessagesSerializer(serializers.ModelSerializer):
    class Meta:
        model=chatMessages
        fields ='__all__'
    

        