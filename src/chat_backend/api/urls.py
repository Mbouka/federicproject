from django.urls import path
from . import views


urlpatterns= [
    #endpoint for signing up
    path('<str:id>',views.login),

    #endpoint for login
    path('signup',views.SignUp),

    #endpoint for adding contact
    path('addContact/<str:phone>',views.createContact),
    
    #endpoint for get all users
    path('users',views.getUsers),

    #endpoint for get a profile contacts
    path('profile/<str:owner>',views.getContacts),

    #endpoint for send message

    path("",views.sendMessage),

    #endpoint for get messages between you and your contact

    path("<str:owner>/<str:contact>",views.readMessage),

    #endpoint for get messages between you and your contact

    path("delete/<str:owner>/<str:contact>",views.deleteContact)

]