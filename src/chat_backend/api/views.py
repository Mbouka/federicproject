
import json
from django.dispatch import receiver
from html5lib import serialize
from rest_framework.response import Response
from .serializers import UserSerializer
from rest_framework.decorators import api_view
from .models import *

import datetime


def settime():
        x = datetime.datetime.now()

        x = str(x)

        x = x.split(" ")

        date = x[0]

        time = x[1]

        return date,time

#Method to get the logged user
def getCurrentUser(id):
    currentUser = UserModel.objects.get(phone=id)

    return currentUser

# api for sign up
@api_view(['POST'])
def SignUp(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


#api for login
@api_view(['GET'])
def login(request,id):
    try:
        user = UserModel.objects.get(phone=id)
        return Response({'login':user.pseudo,'phone':user.phone})
    except UserModel.DoesNotExist:
        return Response({"error":"Not an user"})



#api to create a new contact
@api_view(['POST'])
def createContact(request,phone):
    prop = getCurrentUser(phone)

    contactBook(contactPhone = request.data['contactPhone'],contactName=request.data['contactName'],owner=prop).save()
    
    return Response({"msg":"contact crée"})


@api_view(['POST'])
def sendMessage(request):
    date,time = settime()
    sender = UserModel.objects.get(phone=request.data['sender'])
    receiver = UserModel.objects.get(phone=request.data['receiver'])
    chatMessages(sender=sender,receiver=receiver,body=request.data['body'],sentDate=date,sentTime=time).save()

    return Response({'message':"message envoyé"})


#api to get all user for an owner
@api_view(['GET'])
def getContacts(request,owner):
    owner = getCurrentUser(owner)
    contactsList = []
    c=0
    for contact in contactBook.objects.filter(owner=owner):
        data = {
            "name":contact.contactName,
            "phoneNumber":contact.contactPhone
            }
        contactsList.append(data)
    
    return Response(contactsList)




@api_view(['GET'])
def readMessage(request,owner,contact):
    queryset = chatMessages.objects.filter(sender=owner,receiver=contact).order_by('-sentTime') | chatMessages.objects.filter(sender=contact,receiver=owner).order_by('-sentTime')
    messages = []

    for message in queryset:
        data = {
                'sender':message.sender.phone,
                'receiver':message.receiver.contactPhone,
                'content':message.body,
                'date':message.sentDate,
                'time':message.sentTime
                }
        messages.append(data)
    print(messages)
    return Response(messages)





@api_view(['GET'])
def getUsers(request):
    queryset = UserModel.objects.all()
    serialise_class = UserSerializer(queryset,many=True)

    return Response(serialise_class.data)


@api_view(['POST'])
def deleteContact(request,owner,contact):
    owner = UserModel.objects.get(phone=request.data['sender'])
    contact = UserModel.objects.get(phone=request.data['receiver'])

    contactBook.objects.filter(owner=owner,contactPhone=contact)

    return Response(request.data)




    