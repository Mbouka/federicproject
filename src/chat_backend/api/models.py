from email.policy import default
from django.db import models

import datetime
class UserModel(models.Model):
    phone = models.CharField(max_length=9,primary_key=True)
    firstname = models.CharField(max_length=50,null=False)
    pseudo = models.CharField(max_length=50,null=False)

    def __str__(self):
        return self.pseudo




class contactBook(models.Model):
    owner = models.ForeignKey(UserModel,on_delete=models.CASCADE)
    contact = models.CharField(max_length=9,primary_key=True)
    contactName = models.CharField(max_length=50,null=True)


    def __str__(self):
        return self.contactName



class chatMessages(models.Model):
    sender = models.ForeignKey(UserModel,on_delete=models.CASCADE)
    receiver = models.ForeignKey(contactBook,on_delete=models.CASCADE)
    body = models.TextField()
    seen = models.BooleanField(default=False)
    sentDate = models.CharField(max_length=10)
    sentTime = models.CharField(max_length=20)


    def __str__(self):
        return self.body

